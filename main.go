package main

import (
	"entry_task/consts"
	"entry_task/entry_task_log"
	"entry_task/gRPC"
	"entry_task/routes"
	"github.com/gin-gonic/gin"
	"net/http"
)

var Logger = entry_task_log.GetLogger()

func main() {
	//开启redis连接池
	//1，默认路由
	Logger.Info("use main method start webService")
	r := gin.Default()
	r = routes.CollectRoute(r)
	//如果没有找到相应路由返回指定404页面
	r.NoRoute(func(context *gin.Context) {
		context.HTML(404, "404.html", nil)
	})
	//为全局路由注册中间件
	//r.Use(StatCost())
	//加载HTML模板
	r.LoadHTMLGlob("web_server/html/*")
	//加载静态资源
	r.Static("/css", "static/css/")
	r.Static("/pic", "static/pic/")
	//加载图片
	r.StaticFS("/image", http.Dir("image"))
	//限制上传最大尺寸  上传文件功能
	r.MaxMultipartMemory = 8 << 20
	var err error
	err = gRPC.ConnectGRPC(err)
	if err != nil {
		var context = gin.Context{}
		context.JSON(http.StatusBadRequest, gin.H{
			"message": consts.GRPCClientConnectErrorMessage,
		})
	}
	r.Run()
	//开启web服务
	//web_server.StartWebServe()
}
