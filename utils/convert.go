package utils

import "unsafe"

func Convert(b []byte)  string{
	return *(*string)(unsafe.Pointer(&b))
}
