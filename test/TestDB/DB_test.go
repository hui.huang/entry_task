package TestDB

import (
	"entry_task/db"
	"entry_task/model"
	"fmt"
	"github.com/gin-gonic/gin/binding"
	"testing"
)

func TestFindUserByUsername(t *testing.T) {
	dBase, err, code := db.GetDB()
	if err != nil {
		fmt.Println(err, code)
	}
	username := "huanghui"
	user, err, code := db.FindUserByUsername(dBase, username)
	want := model.UserTab{UserName: "huanghui", UserId: 1, NickName: "111", ProfilePicture: "/image/101595308468_.pic.jpg", Password: "123"}
	if user.UserName != want.UserName && user.UserId != want.UserId { // 因为slice不能比较直接，借助反射包中的方法比较
		t.Errorf("excepted:%v, got:%v", want, user) // 测试失败输出错误提示
	}

}

func TestFindUserByUserId(t *testing.T) {
	dBase := db.StartDB()
	var id int64 = 1
	user := db.FindUserByUserId(dBase, id)
	want := model.UserTab{UserName: "huanghui", UserId: 1, NickName: "111", ProfilePicture: "/image/101595308468_.pic.jpg", Password: "123"}
	if user.UserName != want.UserName && user.UserId != want.UserId { // 因为slice不能比较直接，借助反射包中的方法比较
		t.Errorf("excepted:%v, got:%v", want, user) // 测试失败输出错误提示
	}
}

func TestUploadProfileByUserId(t *testing.T) {
	dBase := db.StartDB()
	var id int64 = 1
	profilePath := "/image/10159530846888888_.pic.jpg"
	db.UploadProfileByUserId(dBase, id, profilePath)
	user := db.FindUserByUserId(dBase, id)
	want := model.UserTab{UserName: "huanghui", UserId: 1, NickName: "111", ProfilePicture: "/image/10159530846888888_.pic.jpg", Password: "123"}
	if user.ProfilePicture != want.ProfilePicture { // 因为slice不能比较直接，借助反射包中的方法比较
		t.Errorf("excepted:%v, got:%v", want, user) // 测试失败输出错误提示
	}

}

func TestAlterNickname(t *testing.T) {
	dBase := db.StartDB()
	var id int64 = 1
	nickname := "huihui"
	db.AlterNickname(dBase, id, nickname)
	user := db.FindUserByUserId(dBase, id)
	if user.NickName != nickname {
		t.Errorf("excepted:%v, got:%v", nickname, user) // 测试失败输出错误提示
	}

}
