package db

import (
	"entry_task/consts"
	entry_task_config "entry_task/entry__task_config"
	"entry_task/entry_task_log"
	"entry_task/model"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
)

var dBase *gorm.DB
var Logger = entry_task_log.GetLogger()
var dbError error

//开启数据库服务
//TODO 连接池（完成）
// init函数在包被调用的时候只会被执行一次
func init() {
	var mysqlConfig entry_task_config.MysqlConfig
	mysqlConfig.GetMysqlConfig()
	Logger.Info("use dbAPI init method open Mysql connect pool")
	db, err := gorm.Open(mysqlConfig.MysqlName, mysqlConfig.MysqlDriver)
	if err != nil {
		Logger.Errorf("连接数据库失败 for %s", "dbAPI startDB method")
		dbError = err
	}
	db.SingularTable(true)        //不加此语句数据库匹配表会加s
	db.DB().SetMaxOpenConns(5000) //设置数据库连接池最大连接数
	db.DB().SetMaxIdleConns(2000) //连接池最大允许的空闲连接数，如果没有sql任务需要执行的连接数大于20，超过的连接会被连接池关闭
	dBase = db
}

func GetDB() (*gorm.DB, error, int32) {
	if dbError != nil {
		return nil, dbError, consts.DBConnectError
	}
	Logger.Info("use dbAPI GetDB method connect MysqlDB")
	return dBase, dbError, 0
}

func StopDB() {
	dBase.Close()
}

//根据用户名查询用户信息
func FindUserByUsername(dBase *gorm.DB, username string) (*model.UserTab, error, int32) {
	Logger.Info("use DbAPI FindUserByUsername method")
	user := new(model.UserTab)
	//start := time.Now()
	err := dBase.Where("user_name = ?", username).Find(user).Error
	if err != nil {
		Logger.Errorf("use DbAPI FindUserByUsername method where method", err)
		return nil, err, consts.DBSelectError
	}
	//log.Println(time.Since(start))  一次查询的时间
	return user, err, 0
}

//根据id查询用户信息
func FindUserByUserId(dBase *gorm.DB, userId int64) (*model.UserTab, error, int32) {
	Logger.Info("use DbAPI FindUserByUserId method")
	user := new(model.UserTab)
	err := dBase.Where("user_id = ?", userId).Find(user).Error
	if err != nil {
		return nil, err, consts.DBSelectError
	}
	return user, err, 0
}

//根据id更新用户信息
func UploadProfileByUserId(dBase *gorm.DB, userId int64, profilePath string) (*model.UserTab, error, int32) {
	Logger.Info("use DbAPI UploadProfileByUserId method")
	user, err, code := FindUserByUserId(dBase, userId)
	if err != nil {
		return nil, err, code
	}
	err = dBase.Model(user).Where("user_id = ?", userId).Update("profile_picture", profilePath).Error
	if err != nil {
		return nil, err, consts.DBUpdateError
	}
	return user, err, 0
}

//根据用户名更改nickname
func AlterNickname(dBase *gorm.DB, userId int64, newNickname string) (*model.UserTab, error, int32) {
	Logger.Info("use DbAPI AlterNickname method")
	user, err, code := FindUserByUserId(dBase, userId)
	if err != nil {
		return nil, err, code
	}
	err = dBase.Model(user).Where("user_id = ?", userId).Update("nick_name", newNickname).Error
	if err != nil {
		return nil, err, consts.DBUpdateError
	}
	return user, err, 0
}
