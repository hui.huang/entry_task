package web_server

import (
	"entry_task/common"
	"entry_task/consts"
	"entry_task/entry_task_log"
	"entry_task/gRPC"
	"entry_task/middleware"
	"fmt"
	"github.com/gin-gonic/gin"
	"log"
	"net/http"
	"time"
)

func StartWebServe() {

	//连接gRPC服务端
	var err error
	gRPC.ConnectGRPC(err)
	entry_task_log.InitLogger()
	//1，默认路由
	r := gin.Default()
	//如果没有找到相应路由返回指定404页面
	r.NoRoute(func(context *gin.Context) {
		context.HTML(404, "404.html", nil)
	})
	//为全局路由注册中间件
	//r.Use(StatCost())
	//加载HTML模板
	r.LoadHTMLGlob("web_server/html/*")
	//加载静态资源
	r.Static("/css", "static/css/")
	r.Static("/pic", "static/pic/")
	//加载图片
	r.StaticFS("/image", http.Dir("image"))
	//限制上传最大尺寸  上传文件功能
	r.MaxMultipartMemory = 8 << 20

	//登陆请求
	r.GET("/login", func(context *gin.Context) {
		entry_task_log.Logger.Info("Trying to hit GET request for %s", "http://localhost:8080/login")
		//gin.H 为一个默认interface
		context.HTML(200, "index.html", gin.H{})
	})

	//上传图片
	r.POST("/submit", middleware.JWTAuthMiddleware(), func(c *gin.Context) {
		entry_task_log.Logger.Infof("Trying to hit POST request for %s", "http://localhost:8080/submit")
		username := c.MustGet("username")
		userId := c.MustGet("userId")
		//根据用户id修改用户的profile
		file, err := c.FormFile("file")
		profilePath := "/image/" + file.Filename
		//fmt.Println(profilePath)
		if err != nil {
			entry_task_log.Logger.Error("上传图片出错 for %s", "http://localhost:8080/submit")
			c.String(500, "上传图片出错")
		}
		gRPC.SubmitProfile(userId.(int64), profilePath, username.(string))
		dst := fmt.Sprintf("/Users/huihuang/go/src/entry_task/image/%s", file.Filename)
		c.SaveUploadedFile(file, dst)
		//c.String(http.StatusOK, file.Filename)
		c.JSON(200, gin.H{
			"status":   "上传成功",
			"username": username,
			"message":  file.Filename})
	})

	//登陆验证用户名和密码
	r.POST("/login", func(con *gin.Context) {
		entry_task_log.Logger.Infof("Trying to hit POST request for %s", "http://localhost:8080/login")
		//创建接收的变量
		//types := con.DefaultPostForm("type", "post")
		//拿到表单中的数据
		username := con.PostForm("username")
		password := con.PostForm("password")
		//调用客户端函数传入给服务端  该函数中内置了链接gRPC客户端代码 并且接收登陆判断结果
		login := gRPC.VerifyLogin(username, password) //login结构体存储登陆反馈信息
		//fmt.Println("整体调用无问题！！")
		//根据登陆反馈信息判断登陆状态
		if login.Message == consts.ONE { //one为 用户不存在
			entry_task_log.Logger.Errorf("用户不存在 to hit POST request for %s", "http://localhost:8080/login")
			con.JSON(http.StatusBadRequest, gin.H{
				"err": consts.ONE,
			})
		}
		if login.Message == consts.TWO { //two为 密码错误
			entry_task_log.Logger.Errorf("密码错误 to hit POST request for %s", "http://localhost:8080/login")
			con.JSON(http.StatusBadRequest, gin.H{
				"err": consts.TWO,
			})
		}
		if login.Message == consts.SERVERERROR { //  服务端错误
			entry_task_log.Logger.Errorf("服务端错误 to hit POST request for %s", "http://localhost:8080/login")
			con.JSON(http.StatusBadRequest, gin.H{
				"err": consts.SERVERERROR,
			})
		}
		//验证成功
		if login.Message == consts.SUCCESS { //登陆成功
			tokenString, _ := common.GenToken(username, login.UserId)
			//将token放入文件

			//设置cooike
			con.SetCookie("token", tokenString, 100, "/", "localhost", false, true)
			//con.JSON(200, gin.H{
			//	"nickname":    login.NickName,
			//	"username":    login.Username,
			//})
			con.HTML(200, "success.html", gin.H{
				"token":       tokenString,
				"profilePath": login.ProfilePath,
				"nickname":    login.NickName,
				"username":    login.Username,
			})
		}
	})
	//返回token信息
	r.GET("/token", middleware.JWTAuthMiddleware(), func(c *gin.Context) {
		entry_task_log.Logger.Infof("trying to hit GET request for %s", "http://localhost:8080/token")
		username := c.MustGet("username")
		userId := c.MustGet("userId")
		token := c.MustGet("token")
		c.JSON(http.StatusOK, gin.H{
			"username": username,
			"userId":   userId,
			"token":    token,
		})
	})
	//修改nickname
	r.GET("/alter", middleware.JWTAuthMiddleware(), func(c *gin.Context) {
		entry_task_log.Logger.Infof("trying to hit GET request for %s", "http://localhost:8080/alter")
		nickname := c.MustGet("nickname")
		c.HTML(http.StatusOK, "alter.html", gin.H{
			"nickname": nickname,
		})
	})

	//更新nickname到数据库
	r.POST("/alter", middleware.JWTAuthMiddleware(), func(c *gin.Context) {
		entry_task_log.Logger.Infof("trying to hit POST request for %s", "http://localhost:8080/alter")
		newNickname := c.PostForm("nickname")
		userId := c.MustGet("userId")
		alter := gRPC.AlterNickname(userId.(int64), newNickname)
		c.JSON(http.StatusOK, gin.H{
			"message":  "修改成功",
			"nickname": alter.NewNickname,
		})
	})
	//运行
	r.Run()
}

//定义中间件
func StatCost() gin.HandlerFunc {
	return func(context *gin.Context) {
		start := time.Now()
		cost := time.Since(start)
		fmt.Print("本次求情花费的时间为：")
		log.Print(cost)
	}
}
