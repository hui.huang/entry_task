package middleware

import (
	"entry_task/common"
	"entry_task/consts"
	"entry_task/db"
	"entry_task/entry_task_log"
	"github.com/gin-gonic/gin"
	"net/http"
)

var Logger = entry_task_log.GetLogger()

func JWTAuthMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		Logger.Info("use middleware JWTAuthMiddleware method")
		authHeader, err := c.Cookie("token")
		if err != nil {
			c.JSON(500, gin.H{
				"code": consts.WebCookieError,
				"msg":  common.GetCodeMessage(consts.WebCookieError),
			})
			c.Abort()
			return
		}
		if authHeader == "" {
			Logger.Errorf("token为空 for middleware  JWTAuthMiddleware method: %v", "middleware")
			c.JSON(500, gin.H{
				"code": consts.WebTokenIsNil,
				"msg":  common.GetCodeMessage(consts.WebTokenIsNil),
			})
			c.Abort()
			return
		}
		token, mc, err := common.ParseToken(authHeader)
		if err != nil && !token.Valid {
			Logger.Errorf("无效的token for middleware  JWTAuthMiddleware method: %v", "middleware")
			c.JSON(500, gin.H{
				"code": consts.WebTokenNOtUseful,
				"msg":  common.GetCodeMessage(consts.WebTokenNOtUseful),
			})
			c.Abort()
			return
		}
		dBase, err, code := db.GetDB()
		if err != nil {
			c.JSON(500, gin.H{
				"code": code,
				"msg":  common.GetCodeMessage(code),
			})
		}
		userId := mc.UserId
		username := mc.Username
		user, err, code := db.FindUserByUserId(dBase, userId)
		if err != nil {
			c.JSON(500, gin.H{
				"code": code,
				"msg":  common.GetCodeMessage(code),
			})
		}
		nickname := user.NickName
		if user.UserName == username {
			// 将当前请求的username信息保存到请求的上下文c上
			c.Set("user", user)
			c.Set("username", username)
			c.Set("userId", userId)
			c.Set("token", authHeader)
			c.Set("nickname", nickname)
			c.Next() // 后续的处理函数可以用过c.Get("username")来获取当前请求的用户信息
		} else {
			Logger.Errorf("token被篡改 for middleware  JWTAuthMiddleware method: %v", "middleware")
			c.JSON(http.StatusBadRequest, gin.H{
				"code": consts.WebTokenIsAlter,
				"msg":  common.GetCodeMessage(consts.WebTokenIsAlter),
			})
		}
	}
}
