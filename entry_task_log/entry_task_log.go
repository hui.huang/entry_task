package entry_task_log


import (
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
	"os"
)

var Logger *zap.SugaredLogger

func GetLogger()  *zap.SugaredLogger{
	return Logger
}

func init() {
	writeSyncer := getLogWriter()
	encoder := getEncoder()
	core := zapcore.NewCore(encoder, writeSyncer, zapcore.DebugLevel)

	logger := zap.New(core, zap.AddCaller())
	Logger = logger.Sugar()
}

func getEncoder() zapcore.Encoder {
	//Encoder:编码器(如何写入日志)。使用开箱即用的NewJSONEncoder()，并使用预先设置的ProductionEncoderConfig()。
	encoderConfig := zap.NewProductionEncoderConfig()
	encoderConfig.EncodeTime = zapcore.ISO8601TimeEncoder
	encoderConfig.EncodeLevel = zapcore.CapitalLevelEncoder
	return zapcore.NewConsoleEncoder(encoderConfig)
}

func getLogWriter() zapcore.WriteSyncer {
	//指定日志将写到哪里去。我们使用zapcore.AddSync()函数并且将打开的文件句柄传进去。
	file, _ := os.Create("./entry_task_log/entry_task.entry_task_log")
	return zapcore.AddSync(file)
}