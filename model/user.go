package model

type UserTab struct {
	UserName string
	NickName string
	ProfilePicture string
	UserId int64
	Password string
}
