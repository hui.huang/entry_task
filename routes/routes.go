package routes

import (
	"entry_task/controller"
	"entry_task/middleware"
	"github.com/gin-gonic/gin"
)

func CollectRoute(r *gin.Engine) *gin.Engine {
	r.GET("/login", controller.LoginGet)
	r.GET("/token", middleware.JWTAuthMiddleware(), controller.TokenGet)
	r.GET("/alter", middleware.JWTAuthMiddleware(), controller.AlterGet)
	r.POST("/login", controller.LoginPost)
	r.POST("/alter", middleware.JWTAuthMiddleware(), controller.AlterPost)
	r.POST("/submit", middleware.JWTAuthMiddleware(), controller.SubmitPost)
	return r
}
