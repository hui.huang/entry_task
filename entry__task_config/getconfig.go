package entry_task_config

import (
	"fmt"
	"gopkg.in/yaml.v2"
	"io/ioutil"
)

type MysqlConfig struct {
	MysqlName   string
	MysqlDriver string
}

type RedisConfig struct {
	RedisNetwork string
	RedisAddress string
}

type RPCConfig struct {
	Port    string
	Network string
}

func (mysqlConfig *MysqlConfig) GetMysqlConfig() *MysqlConfig {

	yamlFile, err := ioutil.ReadFile("entry__task_config/config.yaml")
	if err != nil {
		fmt.Println(err.Error())
	}
	err = yaml.Unmarshal(yamlFile, mysqlConfig)
	if err != nil {
		fmt.Println(err.Error())
	}
	return mysqlConfig
}

func (redisConfig *RedisConfig) GetRedisConfig() *RedisConfig {

	yamlFile, err := ioutil.ReadFile("entry__task_config/redisConfig.yaml")
	if err != nil {
		fmt.Println(err.Error())
	}
	err = yaml.Unmarshal(yamlFile, redisConfig)
	if err != nil {
		fmt.Println(err.Error())
	}
	return redisConfig
}

func (rPCConfig *RPCConfig) GetRPCConfig() *RPCConfig {

	yamlFile, err := ioutil.ReadFile("entry__task_config/RPC.yaml")
	if err != nil {
		fmt.Println(err.Error())
	}
	err = yaml.Unmarshal(yamlFile, rPCConfig)
	if err != nil {
		fmt.Println(err.Error())
	}
	return rPCConfig
}
