package entry_task_config

import "fmt"

func main() {
	var mysqlConfig MysqlConfig
	mysqlConfig.GetMysqlConfig()
	fmt.Println(mysqlConfig)

	var redisConfig RedisConfig
	redisConfig.GetRedisConfig()
	fmt.Println(redisConfig)

	var rpcConfig RPCConfig
	rpcConfig.GetRPCConfig()
	fmt.Println(rpcConfig)
}
