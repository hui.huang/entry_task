package consts

//1错误 数据库错误
//2错误 gRPCClient错误
//3错误 gRPCServer错误
//4错误 webService错误
//5错误 redis错误
//6错误 jwt错误
const (
	//code
	//0表示成功
	WebNoUser         int32 = 40001
	WebPasswordError  int32 = 40002
	WebSuccess        int32 = 40003
	WebCookieError    int32 = 40004
	WebTokenIsNil     int32 = 40005
	WebTokenIsAlter   int32 = 40006
	WebTokenNOtUseful int32 = 40007
	WebGenTokenFail   int32 = 40008
	WebGetFileError   int32 = 40009
	WebSaveFileError  int32 = 40010
	WebGetJsonError   int32 = 40011

	DBSelectError  int32 = 10001
	DBUpdateError  int32 = 10002
	DBConnectError int32 = 10003

	RedisGetError      int32 = 50001
	RedisSetError      int32 = 50002
	RedisConnectError  int32 = 50003
	RedisInitPoolError int32 = 50004
	RedisDelError      int32 = 50005

	GRPCClientConnectError = 20001

	SUCCESS     string = "登陆成功"
	ONE         string = "用户不存在"
	TWO         string = "密码错误"
	SERVERERROR string = "SERVERERROR"

	//message
	WebNoUserMessage         = "数据库中不存在该用户"
	WebPasswordErrorMessage  = "密码错误"
	WebSuccessMessage        = "登陆成功"
	WebCookieErrorMessage    = "获取cookie失败"
	WebTokenIsNilMessage     = "token为空"
	WebTokenIsAlterMessage   = "token被篡改"
	WebTokenNOtUsefulMessage = "token无效"
	WebGenTokenFailMessage   = "生成token失败"
	WebGetFileErrorMessage   = "获取file失败"
	WebSaveFileErrorMessage  = "保存图片失败"
	WebGetJsonErrorMessage   = "对象生成json错误"

	DBSelectErrorMessage  = "查询数据库错误"
	DBUpdateErrorMessage  = "修改数据库错误"
	DBConnectErrorMessage = "连接数据库错误"

	RedisGetErrorMessage      string = "redis GET error"
	RedisSetErrorMessage      string = "redis SET error"
	RedisConnectErrorMessage  string = "redis连接错误"
	RedisInitPoolErrorMessage string = "redis初始化连接池错误"
	RedisDelErrorMessage      string = "redis DEL error"

	GRPCClientConnectErrorMessage = "gRPC连接错误"
)
