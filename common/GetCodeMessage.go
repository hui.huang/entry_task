package common

import (
	"entry_task/consts"
)

func GetCodeMessage(code int32) string {
	switch code {
	case 10001:
		return consts.DBSelectErrorMessage
	case 10002:
		return consts.DBUpdateErrorMessage
	case 10003:
		return consts.DBConnectErrorMessage

	case 20001:
		return consts.GRPCClientConnectErrorMessage

	case 40001:
		return consts.WebNoUserMessage
	case 40002:
		return consts.WebPasswordErrorMessage
	case 40003:
		return consts.WebSuccessMessage
	case 40004:
		return consts.WebCookieErrorMessage
	case 40005:
		return consts.WebTokenIsNilMessage
	case 40006:
		return consts.WebTokenIsAlterMessage
	case 40007:
		return consts.WebTokenNOtUsefulMessage
	case 40008:
		return consts.WebGenTokenFailMessage
	case 40009:
		return consts.WebGetFileErrorMessage
	case 40010:
		return consts.WebSaveFileErrorMessage
	case 40011:
		return consts.WebGetJsonErrorMessage

	case 50001:
		return consts.RedisGetErrorMessage
	case 50002:
		return consts.RedisSetErrorMessage
	case 50003:
		return consts.RedisConnectErrorMessage
	case 50004:
		return consts.RedisInitPoolErrorMessage
	case 50005:
		return consts.RedisDelErrorMessage
	}

	return "未找到此错误信息"
}

/*
	WebNoUser        int32 = 40001
	WebPasswordError int32 = 40002
	WebSuccess       int32 = 40003
	WebCookieError   int32 = 40004

	DBSelectError int32 = 10001
	DBUpdateError int32 = 10002

	RedisGetError int32 = 50001
	RedisSetError int32 = 50002

	GRPCClientConnectError = 20001*/
