package controller

import (
	"entry_task/common"
	"entry_task/consts"
	"entry_task/entry_task_log"
	"entry_task/gRPC"
	"fmt"
	"github.com/gin-gonic/gin"
	"net/http"
)

var Logger = entry_task_log.GetLogger()

func LoginGet(c *gin.Context) {
	Logger.Infof("Trying to hit GET request for controller LoginGet method %s", "http://localhost:8080/login")
	//gin.H 为一个默认interface
	c.HTML(200, "index.html", gin.H{})
}

func AlterGet(c *gin.Context) {
	Logger.Infof("trying to hit GET request for for controller AlterGet method %s", "http://localhost:8080/alter")
	nickname := c.MustGet("nickname")
	c.HTML(http.StatusOK, "alter.html", gin.H{
		"nickname": nickname,
	})
}
func TokenGet(c *gin.Context) {
	Logger.Infof("trying to hit GET request for for controller TokenGet method %s", "http://localhost:8080/token")
	username := c.MustGet("username")
	userId := c.MustGet("userId")
	token := c.MustGet("token")
	c.JSON(http.StatusOK, gin.H{
		"username": username,
		"userId":   userId,
		"token":    token,
	})
}
func LoginPost(c *gin.Context) {
	Logger.Infof("Trying to hit POST request for for controller LoginPost method %s", "http://localhost:8080/login")
	//创建接收的变量
	//types := con.DefaultPostForm("type", "post")
	//拿到表单中的数据
	username := c.PostForm("username")
	password := c.PostForm("password")
	//调用客户端函数传入给服务端  该函数中内置了链接gRPC客户端代码 并且接收登陆判断结果
	login, err := gRPC.VerifyLogin(username, password) //login结构体存储登陆反馈信息
	//fmt.Println("整体调用无问题！！")
	//根据登陆反馈信息判断登陆状态
	if err != nil {
		Logger.Errorf(common.GetCodeMessage(login.Code)+"to hit POST request for for controller LoginPost method %s", "http://localhost:8080/login")
		c.JSON(http.StatusBadRequest, gin.H{
			"code": login.Code,
			"err":  common.GetCodeMessage(login.Code),
		})
	}
	//验证成功
	if login.Code == consts.WebSuccess { //登陆成功
		tokenString, err := common.GenToken(username, login.UserId)
		if err != nil {
			c.JSON(500, gin.H{
				"code":    consts.WebGenTokenFail,
				"message": consts.WebGenTokenFailMessage,
			})
		}
		//将token放入文件
		//设置cooike
		c.SetCookie("token", tokenString, 100, "/", "localhost", false, true)
		c.HTML(200, "success.html", gin.H{
			"token":       tokenString,
			"profilePath": login.ProfilePath,
			"nickname":    login.NickName,
			"username":    login.Username,
		})
	}
}

func SubmitPost(c *gin.Context) {
	Logger.Infof("Trying to hit POST request for for controller SubmitPost method %s", "http://localhost:8080/submit")
	username := c.MustGet("username")
	userId := c.GetInt64("userId")
	//根据用户id修改用户的profile
	file, err := c.FormFile("file")
	if err != nil {
		c.JSON(400, gin.H{
			"code":    consts.WebGetFileError,
			"message": file.Filename + consts.WebGetFileErrorMessage})
	}
	dst := fmt.Sprintf("/Users/huihuang/go/src/entry_task/image/%s", file.Filename)
	err = c.SaveUploadedFile(file, dst)
	if err != nil {
		c.JSON(400, gin.H{
			"code":    consts.WebSaveFileError,
			"message": file.Filename + consts.WebSaveFileErrorMessage})
	}
	profilePath := "/image/" + file.Filename
	submit, err := gRPC.SubmitProfile(userId, profilePath, username.(string))
	if err != nil {
		c.JSON(500, gin.H{
			"code":     submit.Code,
			"username": username,
			"message":  file.Filename})
	}
	//c.String(http.StatusOK, file.Filename)
	c.JSON(200, gin.H{
		"status":   "上传成功",
		"username": username,
		"message":  file.Filename})
}

func AlterPost(c *gin.Context) {
	Logger.Infof("trying to hit POST request for for controller AlterPost method %s", "http://localhost:8080/alter")
	newNickname := c.PostForm("nickname")
	userId := c.GetInt64("userId")
	alter, err := gRPC.AlterNickname(userId, newNickname)
	if err != nil {
		c.JSON(500, gin.H{
			"code":    alter.Code,
			"message": common.GetCodeMessage(alter.Code),
		})
	}
	c.JSON(http.StatusOK, gin.H{
		"message":  "修改成功",
		"nickname": alter.NewNickname,
	})
}
