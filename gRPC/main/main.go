package main

import (
	"entry_task/db"
	"entry_task/gRPC"
	"entry_task/redis_db"
)

func main() {
	//开启gRPC服务端服务
	gRPC.StartGRPCServer()
	defer db.StopDB()
	defer redis_db.GetRedisPool().Close()
	defer gRPC.StopGRPCServer()
}
