package gRPC

import (
	"context"
	"encoding/json"
	"entry_task/consts"
	"entry_task/db"
	entry_task_config "entry_task/entry__task_config"
	"entry_task/model"
	"entry_task/redis_db"
	"github.com/garyburd/redigo/redis"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
	"net"
)

type server struct {
}

var s = grpc.NewServer()

//开启gRPC服务端服务
func StartGRPCServer() {
	// 监听本地的8972端口
	var rpcConfig entry_task_config.RPCConfig
	rpcConfig.GetRPCConfig()
	Logger.Info("user gRPC server StartGRPCServer method to start gRPC server")
	lis, err := net.Listen(rpcConfig.Network, rpcConfig.Port)
	if err != nil {
		Logger.Errorf("failed to listen for gRPC server StartGRPCServer method: %v", err)
		return
	}
	// 创建gRPC服务器
	s = grpc.NewServer()
	//###################每一个服务都需要注册服务！！！！！！！！！！！！！！！ß
	//在gRPC服务端注册服务
	RegisterEntryTaskServer(s, &server{})
	//在给定的gRPC服务器上注册服务器反射服务
	reflection.Register(s)
	// Serve方法在lis上接受传入连接，为每个连接创建一个ServerTransport和server的goroutine。
	// 该goroutine读取gRPC请求，然后调用已注册的处理程序来响应它们。
	err = s.Serve(lis)
	if err != nil {
		Logger.Errorf("failed to server for gRPC server StartGRPCServer method: %v", err)
		return
	}
}

func StopGRPCServer() {
	s.Stop()
}

//服务端实现proto文件中编写的Login服务，经过编译proto文件中所写的服务会生成相应借口
//生成Login接口，此时服务端需要实现Login接口来自定义逻辑操作，实现接口需要指定接收者
//服务端需要做验证操作  验证账号密码的正确性
// loginRequest结构体是在proto文件中定义的 包含请求所携带的数据
//loginResponse结构体是在proto文件中定义的 包含响应所携带的信息
func (s *server) Login(ctx context.Context, in *LoginRequest) (*LoginResponse, error) {
	Logger.Info("use gRPC server Login method")
	//查询使用用户名为key 数据库用户名不可重复
	username := in.Username
	password := in.Password
	//先查询redis中是否存在用户信息  存在就从redis中获取用户  不存在就从mysql中获取并且保存到redis中
	//用户信息  HSet结构   username  {"password": password, "nickname": nickname, "profilePath": profilePath}
	//var user *model.UserTab
	dBase, err, code := db.GetDB()
	if err != nil {
		return &LoginResponse{Code: code}, err
	}
	redisDB := redis_db.GetRedis()
	//if err != nil {
	//	Logger.Error("redis GetRedis error")
	//	return &LoginResponse{Code: code}, err
	//}
	defer redisDB.Close()
	redisPassword, err := redis.String(redisDB.Do("HGet", username, "password"))
	//如果没有查到结果或者查询的不相同，说明缓存中不存在该用户信息
	if err != nil {
		Logger.Infof("缓存中不存在从数据库中取数据并更新到缓存 for %s", "gRPC server Login method")
		//调用数据库中的函数
		user, err, code := db.FindUserByUsername(dBase, username)
		if err != nil {
			return &LoginResponse{Code: code}, err
		}
		//fmt.Println("服务端数据库操作无问题")
		if user.UserName == "" {
			return &LoginResponse{Code: consts.WebNoUser}, err
		} else {
			if user.Password != password {
				//TODO 返回错误码(done)
				return &LoginResponse{Code: consts.WebPasswordError}, err
			}
		}
		//证明该用户密码正确将其放入缓存
		value, _ := json.Marshal(user)
		_, err = redisDB.Do("Set", user.UserName, value)
		if err != nil {
			return &LoginResponse{Code: consts.RedisSetError}, err
		}
		return &LoginResponse{Code: consts.WebSuccess, NickName: user.NickName, UserId: user.UserId, Username: user.UserName, ProfilePath: user.ProfilePicture}, nil
	}
	//用户信息存在 判断密码正确性
	Logger.Infof("缓存中存在数据直接获取 for %s", "gRPC server Login method")
	if redisPassword != password {
		return &LoginResponse{Code: consts.WebPasswordError}, err
	}
	//结构体为值类型，返回结构体的指针也就是地址，减少内存开销
	//TODO 不要重复发网络请求  （完成）
	// 使用json
	var user model.UserTab
	RedisUser, err := redis.Bytes(redisDB.Do("Get", username))
	if err != nil {
		return &LoginResponse{Code: consts.RedisGetError}, err
	}
	json.Unmarshal(RedisUser, &user)
	redisNickname := user.NickName
	redisUserId := user.UserId
	redisProfilePath := user.ProfilePicture
	return &LoginResponse{Code: consts.WebSuccess, NickName: redisNickname, UserId: redisUserId, Username: username, ProfilePath: redisProfilePath}, nil
}

func (s *server) Submit(ctx context.Context, in *SubmitRequest) (*SubmitResponse, error) {
	Logger.Info("use gRPC server submit method")
	dBase, err, code := db.GetDB()
	if err != nil {
		return &SubmitResponse{Code: code}, err
	}
	redisDB := redis_db.GetRedis()
	if err != nil {
		return &SubmitResponse{Code: code}, err
	}
	defer redisDB.Close()
	user, err, code := db.UploadProfileByUserId(dBase, in.UserId, in.ProfilePath)
	if err != nil {
		return &SubmitResponse{Code: code}, err
	}
	//防止数据库缓存的不一致性
	_, err = redisDB.Do("Del", user.UserName)
	if err != nil {
		return &SubmitResponse{Code: consts.RedisSetError}, err
	}
	return &SubmitResponse{ProfilePath: in.ProfilePath}, nil
}

func (s *server) AlterNickname(ctx context.Context, in *AlterRequest) (*AlterResponse, error) {
	Logger.Info("use gRPc server AlterNickname method")
	dBase, err, code := db.GetDB()
	if err != nil {
		return &AlterResponse{Code: code}, err
	}
	redisDB := redis_db.GetRedis()
	if err != nil {
		return &AlterResponse{Code: code}, err
	}
	defer redisDB.Close()
	user, err, code := db.AlterNickname(dBase, in.UserId, in.NewNickname)
	if err != nil {
		return &AlterResponse{Code: code}, err
	}
	_, err = redisDB.Do("Del", user.UserName)
	if err != nil {
		return &AlterResponse{Code: consts.RedisSetError}, err
	}
	return &AlterResponse{NewNickname: in.NewNickname}, nil
}
