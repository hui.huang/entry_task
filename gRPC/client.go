package gRPC

import (
	"context"
	entry_task_config "entry_task/entry__task_config"
	"entry_task/entry_task_log"
	"google.golang.org/grpc"
)

//TODO 使用连接池
var conn *grpc.ClientConn
var c EntryTaskClient
var Logger = entry_task_log.GetLogger()

//gRPC客户端连接服务器
func ConnectGRPC(err error) error {
	var rpcConfig entry_task_config.RPCConfig
	rpcConfig.GetRPCConfig()
	conn, err = grpc.Dial(rpcConfig.Port, grpc.WithInsecure())
	if err != nil {
		Logger.Errorf("fail to connect for %s", "gRPC client connectGRPC method")
		return err
	}
	c = NewEntryTaskClient(conn)
	return err
}

//func GetConnectGRPC()  ( *grpc.ClientConn, error){
//	var rpcConfig entry_task_config.RPCConfig
//	rpcConfig.GetRPCConfig()
//	conn, err := grpc.Dial(rpcConfig.Port, grpc.WithInsecure())
//	if err != nil {
//		Logger.Errorf("fail to connect for %s", "gRPC client connectGRPC method")
//		return nil, err
//	}
//	return conn, nil
//}

func VerifyLogin(username string, password string) (*LoginResponse, error) {
	//login借口调用Login方法  此方法在服务端已经进行了重写  相当于java 向上转型  接口调用实现类的方法
	Logger.Info("use client VerifyLogin method")
	login, err := c.Login(context.Background(), &LoginRequest{Username: username, Password: password})
	if err != nil {
		Logger.Errorf("could not greet for gRPC client VerifyLogin method: %v", err)
		return &LoginResponse{Code: login.Code}, err
	}
	return login, err
}

func SubmitProfile(userId int64, profilePath string, username string) (*SubmitResponse, error) {
	Logger.Info("use client SubmitProfile method")
	submit, err := c.Submit(context.Background(), &SubmitRequest{UserId: userId, ProfilePath: profilePath, Username: username})
	if err != nil {
		Logger.Errorf("could not greet for gRPC client SubmitProfile method: %v", err)
		return &SubmitResponse{Code: submit.Code}, err
	}
	return submit, err
}

func AlterNickname(userId int64, newNickname string) (*AlterResponse, error) {
	Logger.Info("use client AlterNickname method")
	alter, err := c.AlterNickname(context.Background(), &AlterRequest{UserId: userId, NewNickname: newNickname})
	if err != nil {
		Logger.Errorf("could not greet for gRPC client AlterNickname method: %v", err)
		return &AlterResponse{Code: alter.Code}, err
	}
	return alter, err
}
