package redis_db

import (
	entry_task_config "entry_task/entry__task_config"
	"entry_task/entry_task_log"
	"github.com/garyburd/redigo/redis"
	"time"
)

var Logger = entry_task_log.GetLogger()
var redisClient *redis.Pool
var RedisPoolErr error

//TODO Redis 连接池（完成）
//开启redis服务
func init() {
	Logger.Info("use redis_db init method open Redis connect pool")
	// 建立连接池
	var redisConfig entry_task_config.RedisConfig
	redisConfig.GetRedisConfig()
	redisClient = &redis.Pool{
		MaxIdle:     1000, //最大空闲连接数
		MaxActive:   5000, //最大连接数
		IdleTimeout: 1 * time.Second,
		Wait:        true,
		Dial: func() (redis.Conn, error) {
			con, err := redis.Dial(redisConfig.RedisNetwork, redisConfig.RedisAddress)
			if err != nil {
				Logger.Error("redis_db init method connect pool ERROR")
				RedisPoolErr = err
				return nil, err
			}
			return con, nil
		},
	}
}

func GetRedisPool() *redis.Pool {
	return redisClient
}

//func GetRedisError() error {
//	return RedisPoolErr
//}

func GetRedis() redis.Conn {
	Logger.Infof("use GetRedis method")
	//if GetRedisError() != nil {
	//	Logger.Errorf(consts.RedisInitPoolErrorMessage, consts.RedisInitPoolError)
	//	return nil, RedisPoolErr, consts.RedisInitPoolError
	//}
	//err := redisClient.Get().Err()
	//if err != nil {
	//	return nil, err, consts.RedisConnectError
	//}
	return redisClient.Get()
}

//func StartRedis() redis.Conn{
//	c, err := redis.Dial("tcp", "127.0.0.1:6379")
//	//defer c.Close()
//	if err != nil {
//		entry_task_log.Logger.Errorf("Connect 1 to redis_db error for startRedis %v", "redis_db")
//		fmt.Println("Connect 1 to redis_db error", err)
//		return nil
//	}
//	return c
//}
